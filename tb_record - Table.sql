CREATE TABLE `tb_record` (
  `record_id` bigint(20) AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL ,
  `score` int,
  `play_duration` int,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`record_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
