using System;
using MySql.Data.MySqlClient;
using Microsoft.Extensions.Configuration;
public interface IDB
{
        MySqlConnection GetConnection();
}

public class DB : IDB
{
    public string ConnectionString;

    public DB(IConfiguration configuration){
        this.ConnectionString = configuration.GetConnectionString("DefaultConnection");
    }
    
    public MySqlConnection GetConnection()
    {
        //      "DefaultConnection": "server=unity-mydb.mysql.database.azure.com;user id=unity3d@unity-mydb;password=unitycamp!1;persistsecurityinfo=True;port=3306;database=unity"

        // MySqlConnectionStringBuilder conn_string = new MySqlConnectionStringBuilder();
        // conn_string.Server = "unity-mydb.mysql.database.azure.com";
        // conn_string.UserID = "sjhshy@unity-mydb";
        // conn_string.Password = "unitycamp!1";
        // conn_string.Database = "unity";
        // conn_string.Pooling = false;
        // Console.WriteLine(conn_string.ToString());
        MySqlConnection connection = new MySqlConnection
        {
            ConnectionString = this.ConnectionString
        };
        connection.Open();
        return connection;
    }

}