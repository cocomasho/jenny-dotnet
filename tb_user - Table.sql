use unity;

CREATE TABLE `tb_user` (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `facebook_id` varchar(200) DEFAULT '',
  `facebook_name` varchar(200) DEFAULT '',
  `facebook_photo_url` varchar(512) DEFAULT '',
  `facebook_access_token` varchar(200) DEFAULT '',
  `best_score` bigint(999) DEFAULT '',
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8
